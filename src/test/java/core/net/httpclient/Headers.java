package core.net.httpclient;

public interface Headers
{
    String getServer();
    String getContent_type();
    String getConnection();
    String getX_xss_protection();
    String getStrict_transport_security();
    String getCache_control();
}
