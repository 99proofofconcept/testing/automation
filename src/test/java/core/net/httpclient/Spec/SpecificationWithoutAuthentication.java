package core.net.httpclient.Spec;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

public class SpecificationWithoutAuthentication implements Specification
{
    public RequestSpecification create(String baseUri)
    {
        RestAssured.baseURI = baseUri;
        final RequestSpecification request = RestAssured.given()
                .header("Content-Type", "application/json");
        return request;
    }
}
