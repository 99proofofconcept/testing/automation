package core.net.httpclient.Spec;

import io.restassured.specification.RequestSpecification;

public interface Specification
{
    RequestSpecification create(String baseUri);
}
