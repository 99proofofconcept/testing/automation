package core.net.httpclient;


public class BasicHeaders implements Headers
{
    private final String server;
    private final String content_type;
    private final String connection;
    private final String x_xss_protection;
    private final String strict_transport_security;
    private final String cache_control;

    public BasicHeaders(String server,
                        String content_type,
                        String connection,
                        String x_xss_protection,
                        String strict_transport_security,
                        String cache_control) {
        this.server = server;
        this.content_type = content_type;
        this.connection = connection;
        this.x_xss_protection = x_xss_protection;
        this.strict_transport_security = strict_transport_security;
        this.cache_control = cache_control;
    }

    @Override
    public String getServer() { return server;}

    @Override
    public String getContent_type() { return content_type; }

    @Override
    public String getConnection() { return connection; }

    @Override
    public String getX_xss_protection() { return x_xss_protection; }

    @Override
    public String getStrict_transport_security() { return strict_transport_security; }

    @Override
    public String getCache_control() { return cache_control; }
}
