package core.net.httpclient;

import core.net.httpclient.Spec.Specification;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HttpClient
{
    private final Specification specification;
    private String baseUrl;
    private String resource;
    private JSONObject jsonBody;
    private List<Header> basicHeaders;
    private List<Header> newHeaders;

    public HttpClient(Specification specification)
    {
        this.specification = specification;
    }

    public HttpClient addBaseUrl(final String baseUrl){
        this.baseUrl = baseUrl;
        return this;
    }

    public HttpClient addJsonBody(final JSONObject jsonBody){
        this.jsonBody = jsonBody;
        return this;
    }

    public HttpClient addResource(final String resource){
        this.resource = resource;
        return this;
    }

    public HttpClient addHeaders(final List<Header> newHeaders){
        this.newHeaders = newHeaders;
        return this;
    }

    public CustomResponse invoke(Verb verb){

        if (baseUrl == null || baseUrl.trim().isEmpty())
            throw new IllegalArgumentException("BaseUrl needs to be filled");

        RequestSpecification requestSpecification = specification.create(baseUrl);

        Headers headers = getHeaders();

        switch (verb){
            case GET:
                return parseResponse(requestSpecification.get(resource));
            case POST:
                requestSpecification
                        .headers(headers)
                        .body(getBodyParams());
                return parseResponse(requestSpecification.post(resource));
            case DELETE:
                requestSpecification
                        .headers(headers)
                        .body(getBodyParams());
                return parseResponse(requestSpecification.delete(resource));
            case PATCH:
                requestSpecification
                        .headers(headers)
                        .body(getBodyParams());
                return parseResponse(requestSpecification.patch(resource));
        }

        return null;
    }

    protected Headers getHeaders()
    {
        basicHeaders = new ArrayList<>();

        if (newHeaders != null && newHeaders.size() > 0)
        {
            basicHeaders.addAll(newHeaders);
        }

        return new Headers(this.basicHeaders);
    }

    protected String getBodyParams()
    {
        JSONObject requestBodyParams = new JSONObject();

        if (jsonBody != null && !jsonBody.isEmpty())
        {
            requestBodyParams = jsonBody;
        }

        return requestBodyParams.toString();
    }

    protected CustomResponse parseResponse(Response response)
    {
        return new BasicResponse(response);

    }
}
