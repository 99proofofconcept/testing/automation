package test.endpoint.dummyrestapi;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import core.net.httpclient.*;
import core.net.httpclient.Spec.SpecificationWithoutAuthentication;
import org.json.JSONObject;
import test.endpoint.models.UserDTO;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class UsersEndPoint extends BaseEndPoint
{
    private final String token;

    private SpecificationWithoutAuthentication specificationWithoutAuthentication;

    public UsersEndPoint(String baseUrl, String token)
    {
        super(baseUrl);
        this.token = token;
        specificationWithoutAuthentication = new SpecificationWithoutAuthentication();
    }


    public List<UserDTO> getUsers() throws JsonProcessingException
    {
        String resource =  MessageFormat.format("/public/v1/users?access-token={0}", token);

        HttpClient httpClient = new HttpClient(specificationWithoutAuthentication);
        httpClient
                .addBaseUrl(baseUrl)
                .addResource(resource);

        basicResponse = httpClient.invoke(Verb.GET);

        if (basicResponse.getJsonResponse().isEmpty())
            return new ArrayList<>();

        extractJsonNodeByName("data");
        return objectMapper.readValue(jsonNode.toString(), new TypeReference<>() {});
    }

    public CustomResponse getJsonResponseFromUserById(String userId)
    {
        String resource =  MessageFormat.format("/public/v1/users/{0}?access-token={1}",  userId, token);

        HttpClient httpClient = new HttpClient(specificationWithoutAuthentication);
        httpClient
                .addBaseUrl(baseUrl)
                .addResource(resource);

        return httpClient.invoke(Verb.GET);
    }

    public UserDTO createUser(JSONObject bodyParams) throws JsonProcessingException
    {
        String resource =  MessageFormat.format("/public/v1/users?access-token={0}", token);

        HttpClient httpClient = new HttpClient(specificationWithoutAuthentication);
        httpClient
                .addBaseUrl(baseUrl)
                .addResource(resource)
                .addJsonBody(bodyParams);

        basicResponse = httpClient.invoke(Verb.POST);

        if (basicResponse.getJsonResponse().isEmpty())
            return new UserDTO();

        extractJsonNodeByName("data");

        return objectMapper.readValue(jsonNode.toString(), UserDTO.class);
    }

    public Headers getHeadersResponseFromGet()
    {
        String resource =  MessageFormat.format("/public/v1/users?access-token={0}", token);

        HttpClient httpClient = new HttpClient(specificationWithoutAuthentication);
        httpClient
                .addBaseUrl(baseUrl)
                .addResource(resource);

        return httpClient.invoke(Verb.GET).getHeaders();
    }

    public void Dispose()
    {
        basicResponse = null;
    }
}
