package test.endpoint.dummyrestapi;

import core.net.httpclient.CustomResponse;
import core.net.httpclient.HttpClient;
import core.net.httpclient.Spec.SpecificationWithoutAuthentication;
import core.net.httpclient.Verb;

public class HttpBinEndPoint extends BaseEndPoint
{

    private SpecificationWithoutAuthentication specificationWithoutAuthentication;

    public HttpBinEndPoint(String baseUrl)
    {
        super(baseUrl);
        specificationWithoutAuthentication = new SpecificationWithoutAuthentication();
    }

    public CustomResponse getResponseFromWrongResource()
    {
        //Wrong resource
        String wrongResource = "/ge";

        HttpClient httpClient = new HttpClient(specificationWithoutAuthentication);
        httpClient
                .addBaseUrl(baseUrl)
                .addResource(wrongResource);

        return httpClient.invoke(Verb.GET);
    }

    public void Dispose()
    {
        basicResponse = null;
    }
}
