package test.endpoint.listeners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;


public class ListenerRetry implements IRetryAnalyzer
{
    private int count = 0;
    protected int maxAttempt = 3;
    protected long milliseconds = 3000;

    @Override
    public boolean retry(ITestResult result)
    {
        if (!result.isSuccess())
        {
            if (count < maxAttempt)
            {
                count++;
                result.setStatus(ITestResult.FAILURE);
                forceWait();
                return true;
            }
            else
            {
                result.setStatus(ITestResult.FAILURE);
            }
        }
        else
        {
            result.setStatus(ITestResult.SUCCESS);
        }

        return false;
    }

    protected void forceWait()
    {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

