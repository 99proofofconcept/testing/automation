package test.endpoint.exampleretry;

import core.net.httpclient.CustomResponse;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import test.endpoint.BaseTest;
import test.endpoint.dummyrestapi.HttpBinEndPoint;
import test.endpoint.listeners.ListenerRetry;

public class RetryTest extends BaseTest
{
    private HttpBinEndPoint httpBinEndPoint;
    private String baseUrl = "https://httpbin.org";

    @BeforeClass
    public void setUp()
    {
        httpBinEndPoint = new HttpBinEndPoint(baseUrl);
    }


    @Test(retryAnalyzer= ListenerRetry.class)
    public void call_should_be_retried_because_wrong_resource_is_called()
    {
        CustomResponse customResponse = httpBinEndPoint.getResponseFromWrongResource();

        Assert.assertEquals(customResponse.getStatusCode(), 200);
    }

    @Test(retryAnalyzer= ListenerRetry.class)
    public void response_status_should_match_with_404_status_code()
    {
        CustomResponse customResponse = httpBinEndPoint.getResponseFromWrongResource();

        Assert.assertEquals(customResponse.getStatusCode(), 404);
    }

    @AfterClass
    public void tearDown()
    {
        httpBinEndPoint.Dispose();
        httpBinEndPoint = null;
    }
}
