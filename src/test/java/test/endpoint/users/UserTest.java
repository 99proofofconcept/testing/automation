package test.endpoint.users;

import com.fasterxml.jackson.core.JsonProcessingException;
import core.net.httpclient.Headers;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import test.endpoint.BaseTest;
import test.endpoint.dummyrestapi.UsersEndPoint;;
import test.endpoint.models.UserDTO;

import java.util.List;

public class UserTest extends BaseTest
{
    private UsersEndPoint usersEndPoint;
    private String baseUrl = "https://gorest.co.in";


    @BeforeClass
    public void setUp()
    {
        usersEndPoint = new UsersEndPoint(baseUrl, token);
    }


    @Test
    public void all_users_should_be_retrieved() throws JsonProcessingException
    {
        List<UserDTO> users = usersEndPoint.getUsers();

        Assert.assertEquals(usersEndPoint.getResponseStatusCode(), 200);
        Assert.assertEquals(users.size(), 20);
    }

    @Test
    public void all_users_should_have_email_fulfilled() throws JsonProcessingException
    {
        List<UserDTO> users = usersEndPoint.getUsers();

        users.forEach((userDTO) ->
        {
            Assert.assertTrue( userDTO.getId() > 0, "User without identifier was found");
            Assert.assertTrue(!userDTO.getEmail().isEmpty(), "User without email was found");
        });
    }


    @Test
    public void user_creation_should_be_created_properly() throws JsonProcessingException
    {
        String email = "ValeriaSM@hotmail.com";

        JSONObject bodyParams = new JSONObject();
        bodyParams.put("name", "Valeria");
        bodyParams.put("gender", "Female");
        bodyParams.put("email", email);
        bodyParams.put("status", "Active");

        UserDTO userDTO = usersEndPoint.createUser(bodyParams);

        Assert.assertEquals(usersEndPoint.getResponseStatusCode(), 201);
        Assert.assertEquals(userDTO.getEmail(), email);
        Assert.assertTrue(userDTO.getId() > 1);
    }


    @Test
    public void get_response_headers_should_match_with_expected_response_specifications()
    {
        Headers headers = usersEndPoint.getHeadersResponseFromGet();

        Assert.assertEquals(headers.getConnection(), "keep-alive");
        Assert.assertEquals(headers.getCache_control(), "max-age=0, private, must-revalidate");
        Assert.assertEquals(headers.getContent_type(), "application/json; charset=utf-8");
        Assert.assertEquals(headers.getServer(), "nginx");
        Assert.assertEquals(headers.getStrict_transport_security(), "max-age=63072000; includeSubDomains");
        Assert.assertEquals(headers.getX_xss_protection(), "1; mode=block");
    }

    @AfterClass
    public void tearDown()
    {
        usersEndPoint.Dispose();
        usersEndPoint = null;
    }
}
