## Rest API test

### Strategy
```
Isolate tests through a custom client, so there is no direct dependencies. 
We might change the framework we use in order to access to the REST Api 
and test wouldn't be affected
```

### Basic goals
```
- Test happy path retrieving a list of users
- Test some user's data were fulfilled
- Test some headers response
- Test JsonSchema. (In a real scenario UserSpec.json should be on the cloud)
```

### Tools
```
REST Assured
TestNG
Everit Json Schema
```


