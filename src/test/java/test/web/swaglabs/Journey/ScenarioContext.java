package test.web.swaglabs.Journey;

import java.util.HashMap;
import java.util.Map;

public class ScenarioContext
{
    private Map<String, Object> scenarioContext;

    public ScenarioContext()
    {
        scenarioContext = new HashMap<>();
    }

    public void setContext(String key, Object value)
    {
        scenarioContext.put(key.toLowerCase(), value);
    }

    public Object getContext(String key)
    {
        return scenarioContext.get(key.toLowerCase());
    }

    public Boolean isContains(String key)
    {
        return scenarioContext.containsKey(key.toLowerCase());
    }
}
