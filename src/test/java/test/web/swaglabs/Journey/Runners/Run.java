package test.web.swaglabs.Journey.Runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features = "src/test/resources/features",
        glue = {"test.web.swaglabs.Journey.StepsDefinitions"}
)
public class Run extends AbstractTestNGCucumberTests
{
}
