package test.web.swaglabs.Journey;

import org.openqa.selenium.WebDriver;
import test.web.swaglabs.Driver;


public class TestContext
{
    private Driver driver;
    private WebDriver webDriver;
    private ScenarioContext scenarioContext;

    public TestContext()
    {
        driver = new Driver();
        driver.setup();

        scenarioContext = new ScenarioContext();
    }

    public Driver getWebDriver()
    {
        return driver;
    }

    public ScenarioContext getScenarioContext()
    {
        return scenarioContext;
    }

    public void dispose()
    {
        driver.dispose();
    }
}
