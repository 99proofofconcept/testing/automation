package test.web.swaglabs.E2E;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import test.web.swaglabs.Driver;


public class BaseTest
{
    //TODO: review webDriver variable
    protected Driver driver;

    @BeforeClass
    protected void setup()
    {
        driver = new Driver();
        driver.setup();
    }


    @AfterClass
    public void tearDown()
    {
       driver.dispose();
    }
}
