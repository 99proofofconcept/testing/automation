package test.web.swaglabs.E2E;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import test.web.swaglabs.pages.LoginPage;


public class LoginTest extends BaseTest
{
    private String loginPageUrl = "https://www.saucedemo.com/";

    @BeforeMethod
    public void setUp()
    {
        driver.getDriver().navigate().to(loginPageUrl);
    }


    @Test
    public void user_with_right_credentials_should_land_in_product_page_after_do_login()
    {
        LoginPage loginPage = new LoginPage(driver.getDriver());

        Boolean hasLandedInProductPage =
                loginPage
                    .writeUser("standard_user")
                    .writePassword("secret_sauce")
                    .doLogin()
                    .isProductPageVisible();

        Assert.assertTrue(hasLandedInProductPage);
    }

    @Test
    public void message_error_should_be_showed_when_user_locked_do_login()  {
        LoginPage loginPage = new LoginPage(driver.getDriver());

        loginPage
                .writeUser("locked_out_user")
                .writePassword("secret_sauce")
                .doLogin();

        Assert.assertTrue(loginPage.isVisibleMessageError());
    }
}
