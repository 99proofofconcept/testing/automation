package test.web.swaglabs.E2E;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import test.web.swaglabs.pages.MockLoginPage;
import test.web.swaglabs.pages.ProductsPage;


public class JourneyTest extends BaseTest
{

    @BeforeMethod
    public void setUp()
    {
        MockLoginPage mockLoginPage = new MockLoginPage(driver.getDriver());
        mockLoginPage.giveMeAuthorization();
    }

    @Test
    public void doJourney()
    {
        ProductsPage productsPage = new ProductsPage(driver.getDriver());

        Boolean isCheckoutComplete = productsPage
                .addSauceLabsProduct()
                .addSauceLabsBikeLight()
                .clickShoppingButton()
                .clickCheckout()
                .fillFirstName("Test")
                .fillLastName("Journey")
                .fillPostalCode("2100")
                .doContinue()
                .clickFinish()
                .isCheckoutCompletePageVisible();

        Assert.assertTrue(isCheckoutComplete);
    }
}
