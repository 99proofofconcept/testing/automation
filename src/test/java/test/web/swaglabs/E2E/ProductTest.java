package test.web.swaglabs.E2E;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import test.web.swaglabs.pages.MockLoginPage;
import test.web.swaglabs.pages.ProductsPage;

import java.util.List;

public class ProductTest extends BaseTest
{

    @BeforeMethod
    public void setUp()
    {
        MockLoginPage mockLoginPage = new MockLoginPage(driver.getDriver());
        mockLoginPage.giveMeAuthorization();
    }


    @Test
    public void all_expected_products_should_be_contained_in_the_page()
    {
        ProductsPage productsPage = new ProductsPage(driver.getDriver());
        List<WebElement> products = productsPage.getInventoryItems();

        Assert.assertEquals(products.size(), 6);
    }

    @Test
    public void all_products_should_show_the_price()
    {
        ProductsPage productsPage = new ProductsPage(driver.getDriver());
        List<String> productPrices = productsPage.getInventoryItemPrices();

        productPrices.forEach((price) ->
        {
            Assert.assertTrue(!price.equals("0.0") && !price.isEmpty(),
                    "The page contains an empty price");
        });
    }

    @Test
    public void after_logout_I_should_land_in_login_page()
    {
        ProductsPage productsPage = new ProductsPage(driver.getDriver());

        Boolean amIInLoginPage = productsPage.
                clickButtonMenu()
                .doLogout()
                .amIInLoginPage();

        Assert.assertTrue(amIInLoginPage);
    }
}
