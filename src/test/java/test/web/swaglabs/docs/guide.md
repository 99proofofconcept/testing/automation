## Web test

### Strategy
```
E2E test     
Journey test (UAT)
```

### Basic goals
```
- Basic test of the behavior of the UI
- Basic test of one User Acceptance Test
```

### Tools
```
Selenium
TestNG
Cucumber/Gherkin
```


