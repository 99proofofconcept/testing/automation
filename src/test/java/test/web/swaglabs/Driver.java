package test.web.swaglabs;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import test.web.swaglabs.E2E.BaseTest;

import java.net.URL;

public class Driver
{
    private WebDriver driver;

    public WebDriver getDriver()
    {
        return driver;
    }

    public void setup()
    {
        setProperties();
        FirefoxOptions options = new FirefoxOptions();
        options.setLogLevel(FirefoxDriverLogLevel.TRACE);
        driver = new FirefoxDriver(options);
        driver.manage().window().maximize();
    }

    public void dispose()
    {
        try
        {
            driver.close();
            driver.quit();
        }
        catch (Exception e)
        {
            //Intentionally avoided
        }
    }

    private void setProperties()
    {
        URL url = Driver.class.getClassLoader().getResource("browsers/geckodriver.exe");
        String path = url.getPath();
        System.setProperty("webdriver.gecko.driver", path);
    }
}
