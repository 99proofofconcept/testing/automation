package test.web.swaglabs.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage
{

    public LoginPage(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.CLASS_NAME, using = "login_wrapper")
    WebElement loginSection;

    @FindBy(how = How.XPATH,using = "//*[@id=\"login_button_container\"]/div/form/div[3]")
    WebElement messageContainerError;

    @FindBy(how = How.ID,using = "user-name")
    WebElement userName;

    @FindBy(how = How.ID,using = "password")
    WebElement password;

    @FindBy(how = How.ID,using = "login-button")
    WebElement loginButton;


    public boolean amIInLoginPage()
    {
        return loginSection.isDisplayed();
    }

    public boolean isVisibleMessageError()
    {
        return messageContainerError.isDisplayed();
    }

    public LoginPage writeUser(String user)
    {
        userName.clear();
        userName.sendKeys(user);
        return this;
    }

    public LoginPage writePassword(String keySecret)
    {
        password.clear();
        password.sendKeys(keySecret);
        return this;
    }

    public ProductsPage doLogin()
    {
        loginButton.click();
        return new ProductsPage(driver);
    }
}
