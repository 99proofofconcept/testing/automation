package test.web.swaglabs.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class MenuPage extends BasePage
{

    protected MenuPage(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "logout_sidebar_link")
    WebElement logout;

    public LoginPage doLogout()
    {
        logout.click();
        return new LoginPage(driver);
    }
}
