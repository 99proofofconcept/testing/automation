package test.web.swaglabs.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HeaderComponent extends BasePage
{

    public HeaderComponent(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.CLASS_NAME, using = "bm-burger-button")
    WebElement burgerButton;

    @FindBy(how = How.CLASS_NAME, using = "shopping_cart_link")
    WebElement shoppingCard;



    public MenuPage clickButtonMenu()
    {
        burgerButton.click();
        return new MenuPage(driver);
    }

    public YourCartPage clickShoppingCard()
    {
        shoppingCard.click();
        return new YourCartPage(driver);
    }

}
