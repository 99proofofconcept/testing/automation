package test.web.swaglabs.pages.CheckoutPages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import test.web.swaglabs.pages.BasePage;
import test.web.swaglabs.pages.CheckoutPages.CheckoutCompletePage;

public class CheckoutOverviewPage extends BasePage
{

    public CheckoutOverviewPage(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "finish")
    WebElement finish;


    public CheckoutCompletePage clickFinish()
    {
        JavascriptExecutor js = (JavascriptExecutor) driver;

        js.executeScript("arguments[0].scrollIntoView();", finish);

        finish.click();

        return new CheckoutCompletePage(driver);
    }
}
