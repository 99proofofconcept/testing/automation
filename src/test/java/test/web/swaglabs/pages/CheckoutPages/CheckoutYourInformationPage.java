package test.web.swaglabs.pages.CheckoutPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import test.web.swaglabs.pages.BasePage;

public class CheckoutYourInformationPage extends BasePage
{

    public CheckoutYourInformationPage(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "first-name")
    WebElement firstname;

    @FindBy(how = How.ID, using = "last-name")
    WebElement lastname;

    @FindBy(how = How.ID, using = "postal-code")
    WebElement postalCode;

    @FindBy(how = How.ID, using = "continue")
    WebElement continueButton;



    public CheckoutYourInformationPage fillFirstName(String value)
    {
        firstname.clear();
        firstname.sendKeys(value);
        return this;
    }

    public CheckoutYourInformationPage fillLastName(String value)
    {
        lastname.clear();
        lastname.sendKeys(value);
        return this;
    }

    public CheckoutYourInformationPage fillPostalCode(String value)
    {
        postalCode.clear();
        postalCode.sendKeys(value);
        return this;
    }

    public CheckoutOverviewPage doContinue()
    {
        continueButton.click();
        return new CheckoutOverviewPage(driver);
    }
}
