package test.web.swaglabs.pages.CheckoutPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import test.web.swaglabs.pages.BasePage;

public class CheckoutCompletePage extends BasePage
{
    public CheckoutCompletePage(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.ID, using = "checkout_complete_container")
    WebElement checkoutContainer;


    public boolean isCheckoutCompletePageVisible()
    {
        return checkoutContainer.isDisplayed();
    }
}
