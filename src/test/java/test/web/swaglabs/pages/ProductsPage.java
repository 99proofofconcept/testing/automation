package test.web.swaglabs.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.stream.Collectors;

public class ProductsPage extends BasePage
{

    public ProductsPage(WebDriver driver)
    {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.CLASS_NAME, using = "inventory_list")
    WebElement inventoryList;

    @FindBy(how = How.CLASS_NAME, using = "inventory_item")
    List<WebElement> inventoryItems;

    @FindBy(how = How.CLASS_NAME, using = "inventory_item_price")
    List<WebElement> inventoryItemPrices;

    @FindBy(how = How.ID, using = "add-to-cart-sauce-labs-backpack")
    WebElement sauceLabsBackpack;

    @FindBy(how = How.ID, using = "add-to-cart-sauce-labs-bike-light")
    WebElement sauceLabsBackpackLight;


    public boolean isProductPageVisible()
    {
        return inventoryList.isDisplayed();
    }

    public List<WebElement> getInventoryItems()
    {
        return inventoryItems;
    }

    public List<String> getInventoryItemPrices()
    {
        return inventoryItemPrices.stream()
                .map(p ->  p.getText().replace("$", ""))
                .collect(Collectors.toList());
    }

    public MenuPage clickButtonMenu()
    {
        HeaderComponent headerComponent = new HeaderComponent(driver);
        return headerComponent.clickButtonMenu();
    }

    public YourCartPage clickShoppingButton()
    {
        HeaderComponent headerComponent = new HeaderComponent(driver);
        return headerComponent.clickShoppingCard();
    }

    public ProductsPage addSauceLabsProduct()
    {
        sauceLabsBackpack.click();
        return this;
    }

    public ProductsPage addSauceLabsBikeLight()
    {
        sauceLabsBackpackLight.click();
        return this;
    }
}
