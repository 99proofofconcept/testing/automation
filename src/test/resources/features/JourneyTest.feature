Feature: As a user I want to buy a couple of product
  Scenario: User should land in Pony Express page when the purchase has finished
    Given I login
    And select a couple of products
    And do checkout
    And fill the information out
    When I finish the purchase
    Then the checkout has been completed

